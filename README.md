# WooCommerce Bought Product Tab

WooCommerce plugin that adds a new tab to all products, but it's contents are only shown to accounts who have ever purchased the product.

All products get a new WYSIWYG editor on their edit screen. Whatever is input in it will be shown as a tab on the product page. If a guest clicks the tab, all that'll be visible is a message that its contents are only available to those who have purchased the product, whereas a logged in user with an ID that matches a placed order containing the product, will be served the content instead.